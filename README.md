# CargoCult

CargoCult is designed to print tote tags and shipping tags on demand. In many multi-branch libraries, there's often a courier system that transports library materials from branch to branch. These are often shipped in plastic tote bins and the bins are marked with removable tags that state their destination.  Libraries will typically have a bunch of tags that they'll need to dig through when marking the bins for shipment. Either that, or they'll make a tag on the fly using a marker and scratch paper.

This makes the outgoing shipping process take longer. It's far easier to generate tote tags on demand.

CargoCult solves this by presenting an easy to use interface that is touch-screen friendly. It displaus the names of your branch abbreviations on large buttons. If you happen to work in an environment where you're shipping to other library systems, CargoCult can generate tote tags for those too, along with shipping tags to place in the items.

CargoCult works best with a computer attached to a thermal receipt printer. We recommend setting it up in a kiosk mode in Firefox, Chrome, or Chromium so you can disable the print confirmation and have it go straight to the receipt printer without needing to confirm the print job.

Usage is simple. Select whether you want to print tote tags or shipping tags and then click/touch the buttons to print your tag. CargoCult will print a tag that's uniform, easy to read, and displays the branch abbreviation, the full branch name, and the date the tag was printed.

---

### Questions?

Do you have questions or need help with setup? I'd be happy to help! Contact:

cyberpunklibrarian (at) protonmail (dot) com
