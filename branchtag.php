<head><link rel="stylesheet" type="text/css" href="binslip.css"></head>
  <body onload="printSlip(); newDoc();">
  <div id="binslip">

<?php

include 'creds.php';

date_default_timezone_set('America/Phoenix');

$timestamp = date('M j, Y');

$branch = htmlspecialchars($_GET["tag"]);

$result = mysqli_query($conn,"SELECT * FROM branches WHERE branchshort = '$branch'");

while($row = mysqli_fetch_array($result))
{
  $row_branchshort = $row['branchshort'];
  $row_branchfull = $row['branchfull'];

  echo '<center><h1>'.$row_branchshort.'</h1>';
  echo '<p>'.$row_branchfull.'<br>'.$timestamp.'</p></center>';
  echo '<br><br><br>.';

}

?>
</div> <!-- End binslip -->

<script>function printSlip() {window.print()}</script>

<script>function newDoc() {window.location.assign("totetag.php")}</script>

</body>
