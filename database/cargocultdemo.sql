-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 23, 2018 at 12:57 PM
-- Server version: 10.1.30-MariaDB-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cargocultdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `recordID` int(11) NOT NULL,
  `branchfull` tinytext NOT NULL,
  `branchshort` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Branch list';

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`recordID`, `branchfull`, `branchshort`) VALUES
(1, 'Collins Branch Library', 'COL'),
(2, 'Kelly Branch Library', 'KEL'),
(3, 'Baez Branch Library', 'BAE'),
(4, 'Germanotta Regional Library', 'GER'),
(5, 'Evangelos Branch Library', 'EVA'),
(6, 'Baker Regional Library', 'BAK'),
(7, 'Sudo Branch Library', 'SUD'),
(8, 'Bourdin Branch Library', 'BOU'),
(9, 'Monae Branch Library', 'MON'),
(10, 'Smith Branch Library', 'SMI'),
(11, 'Clancy Branch Library', 'CLA '),
(12, 'Hawkins Branch Library', 'HAW'),
(13, 'Rollins Branch Library', 'ROL'),
(14, 'Bowie Branch Library', 'BOW'),
(15, 'Amuro Branch Library', 'AMU'),
(16, 'Harry Branch Library', 'HAR'),
(17, 'Crothers Branch Library', 'CRO'),
(18, 'Wagner Regional Library', 'WAG');

-- --------------------------------------------------------

--
-- Table structure for table `libraries`
--

CREATE TABLE `libraries` (
  `recordID` int(11) NOT NULL,
  `libraryfull` tinytext NOT NULL,
  `libraryshort` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Other community libraries';

--
-- Dumping data for table `libraries`
--

INSERT INTO `libraries` (`recordID`, `libraryfull`, `libraryshort`) VALUES
(10, 'Parkerfield Public Library', 'PARK'),
(11, 'Barrington Public Library', 'BARR'),
(12, 'Lynnport Public Library', 'LYNN'),
(13, 'Knightsford Community College', 'KNIT'),
(14, 'Hartleydale Public Library', 'HART'),
(15, 'Blue Ridge Library System', 'BLUE'),
(16, 'Leeburg Public Library', 'LEEB'),
(17, 'North Heights Public Library', 'NORT'),
(18, 'Sable County Library District', 'SCLD');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`recordID`);

--
-- Indexes for table `libraries`
--
ALTER TABLE `libraries`
  ADD PRIMARY KEY (`recordID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `recordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `libraries`
--
ALTER TABLE `libraries`
  MODIFY `recordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
