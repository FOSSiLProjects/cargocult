<html>
<head>
  <title>CargoCult - Select Library</title>
  <link rel="stylesheet" type="text/css" href="cargocult.css">
</head>
<body>
  <h1>CargoCult - Shipping Tag Generator</h1>
  <h3>Select your library</h3>
  <hr>
<?php
include 'creds.php';

$libresult = mysqli_query($conn,"SELECT libraryshort FROM libraries ORDER BY 1");

while($row = mysqli_fetch_array($libresult))
{
  $row_libraryshort = $row['libraryshort'];

  echo '<a href="shipping.php?shiplibrary='.$row_libraryshort.'" class="library">'.$row_libraryshort.'</a>';

}

mysqli_close($conn);
// Close database connection.

?>

<hr>
<br>
<a class="select" href="index.html">Start Over</a>

</body>
</html>
