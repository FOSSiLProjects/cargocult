<html>
<head>
  <title>CargoCult - Shipping Tag Generator</title>
  <link rel="stylesheet" type="text/css" href="cargocult.css">
</head>
<body>
  <h1>CargoCult - Shipping Tag Generator</h1>

  <?php

  $shiplibrary = htmlspecialchars($_GET["shiplibrary"]);

  echo '<h2>Shipping from: '.$shiplibrary.'</h2>';

  include 'creds.php';

  echo '<br><hr><h3>Ship items to:</h3>';

  $libresult = mysqli_query($conn,"SELECT libraryshort FROM libraries ORDER BY 1");

  while($row = mysqli_fetch_array($libresult))
  {
    $row_libraryshort = $row['libraryshort'];

    echo '<a href="shippingtag.php?shiplibrary='.$shiplibrary.'&destlibrary='.$row_libraryshort.'" class="library">'.$row_libraryshort.'</a>';

  }

  // Close database connection.
  mysqli_close($conn);
   ?>

   <br><hr>
   <a class="select" href="libraryselect.php">Select Library</a> <a class="select" href="index.html">Start Over</a>



</body>
</html>
