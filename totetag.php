<html>
<head>
  <title>CargoCult - Tote Tag Generator</title>
  <link rel="stylesheet" type="text/css" href="cargocult.css">
</head>
<body>
  <h1>CargoCult - Tote Tag Generator</h1>
  <hr>
  <h3>SCLD Branches</h3>

  <?php
  include 'creds.php';

  // Run query and loop through the column to write the file
  $result = mysqli_query($conn,"SELECT branchshort FROM branches ORDER BY 1");

  while($row = mysqli_fetch_array($result))
  {
    $row_branchshort = $row['branchshort'];

    echo '<a href="branchtag.php?tag='.$row_branchshort.'" class="branch">'.$row_branchshort.'</a>';

  }

  echo '<br><hr><h3>Community Libraries</h3>';

  $libresult = mysqli_query($conn,"SELECT libraryshort FROM libraries ORDER BY 1");

  while($row = mysqli_fetch_array($libresult))
  {
    $row_libraryshort = $row['libraryshort'];

    echo '<a href="librarytag.php?tag='.$row_libraryshort.'" class="library">'.$row_libraryshort.'</a>';

  }

  // Close database connection.
  mysqli_close($conn);
   ?>

   <hr>
   <br>
   <a class="select" href="index.html">Start Over</a>



</body>
</html>
